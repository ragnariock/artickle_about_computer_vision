from predict import * 
import cv2
import os
import numpy as np

PATH_TO_MODEL_FOLDER="model/"
NAME_MODEL="frozen_inference_graph.pb"
PATH_TO_PBTXT="model/detection.pbtxt"

pr_object=predict_image(PATH_TO_MODEL_FOLDER,NAME_MODEL,PATH_TO_PBTXT)





def predict(path_to_image, path_to_save=""):
    image = cv2.imread(path_to_image)
    result_data, _, _ = pr_object.predict_image(image=image)
    if not path_to_image == "":
        cv2.imwrite(path_to_save,result_data)
    cv2.imshow("result : ", result_data)
    cv2.waitKey(0)


path_to_image = "images/7.jpg" 
path_to_save = "results/7.jpg" 

predict(path_to_image, path_to_save=path_to_save)