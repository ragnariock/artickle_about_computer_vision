from PIL import Image
import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
import cv2
from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from utils import label_map_util



class predict_image:
    def __init__(self,MODEL_NAME,PATH_TO_FROZEN_GRAPH,PATH_TO_LABELS):
        self.MODEL_NAME=MODEL_NAME
        self.PATH_TO_FROZEN_GRAPH=self.MODEL_NAME + PATH_TO_FROZEN_GRAPH
        self.PATH_TO_LABELS=PATH_TO_LABELS
        self.detection_graph=tf.Graph()
        self.category_index = {1: {'id': 1, 'name': 'raccon'}} 
        self.load_grath()
    def load_grath(self):
        #detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.PATH_TO_FROZEN_GRAPH, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
    
    def predict_image(self, wall_of_image=0.7, color=(255, 0, 0), path_to_img=None, image=None):
        with self.detection_graph.as_default():
            with tf.Session() as sess:
                    print("self.category_index : ",self.category_index)
                    # Get handles to input and output tensors
                    ops = tf.get_default_graph().get_operations()
                    all_tensor_names = {output.name for op in ops for output in op.outputs}
                    tensor_dict = {}
                    for key in [
                      'num_detections', 'detection_boxes', 'detection_scores',
                      'detection_classes', 'detection_masks'
                    ]:
                        tensor_name = key + ':0'
                        if tensor_name in all_tensor_names:
                            tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
                          tensor_name)
                    if not path_to_img == None :
                        image_np = cv2.imread(path_to_img, 1)
                    elif path_to_img == None:
                        image_np = image
                    else:
                        raise Exception("PATH IS NOT RIGHT")
                    image_np = cv2.resize(image_np, (300, 300))
                    image_np_expanded = np.expand_dims(image_np, axis=0)
                    if 'detection_masks' in tensor_dict:
                        # The following processing is only for single image
                        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
                        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
                        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
                        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
                        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
                        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
                        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
                            detection_masks, detection_boxes, image.shape[0], image.shape[1])
                        detection_masks_reframed = tf.cast(
                            tf.greater(detection_masks_reframed, 0.5), tf.uint8)
                        # Follow the convention by adding back the batch dimension
                        tensor_dict['detection_masks'] = tf.expand_dims(
                            detection_masks_reframed, 0)
                    image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

                    # Run inference
                    output_dict = sess.run(tensor_dict,
                                            feed_dict={image_tensor: np.expand_dims(image_np, 0)})

                    # all outputs are float32 numpy arrays, so convert types as appropriate
                    output_dict['num_detections'] = int(output_dict['num_detections'][0])
                    output_dict['detection_classes'] = output_dict[
                        'detection_classes'][0].astype(np.uint8)
                    output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
                    output_dict['detection_scores'] = output_dict['detection_scores'][0]
                    if 'detection_masks' in output_dict:
                        output_dict['detection_masks'] = output_dict['detection_masks'][0]
                    scores_old =  output_dict['detection_scores']
                    classes_old = output_dict['detection_classes']
                    boxes_old=output_dict['detection_boxes']
                    scores_new=[]
                    classes_new=[]
                    boxes_new=[]
                    name_tittle=''
                    for i in range(len(scores_old)):
                        if scores_old[i] > wall_of_image:
                            scores_new.append(scores_old[i])
                            classes_new.append(classes_old[i])
                            boxes_new.append(boxes_old[i])
                    print("scores : ",scores_new)
                    print("classes : ",classes_new)
                    print("category_index : ",self.category_index)
                    for i in range(len(scores_new)):
                        scores_new[i]=scores_new[i]+10
                    
                    boxes_new_arr=[]
                    for i in range(len(boxes_new)):
                        tmp_arr=[]
                        for j in range(len(boxes_new[i])):
                            tmp_arr.append(int(boxes_new[i][j] * 300))
                        boxes_new_arr.append(tmp_arr)
                    image=None
                    for i in range(len(boxes_new_arr)):
                        box=boxes_new_arr[i]
                        name_tittle+=self.category_index[classes_new[i]]['name'] + " + "
                        #print(box)
                        image = cv2.rectangle(image_np, (box[1],box[0]), (box[3],box[2]), color, 2)
                        continue
                    font = cv2.FONT_HERSHEY_SIMPLEX 
                    org = (0, 20) 
                    fontScale = 0.5
                    color = (255, 0, 0)
                    thickness = 1
                    image_result = cv2.putText(image, name_tittle, org, font,  fontScale, color, thickness, cv2.LINE_AA)
                    if image_result is None:
                        print("SUKA BLATY")
                        return image_np, name_tittle, scores_new
                    return image_result, name_tittle, scores_new




